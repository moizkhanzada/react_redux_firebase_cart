import React, { Component } from 'react';
import fire from '../config/firebase';
import './css/checkout.css';
import Input from 'react-toolbox/lib/input/Input';
import Button from 'react-toolbox/lib/button/Button';
import { withRouter } from 'react-router-dom';

class PaymentInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cardNumber: '',
      ExpirationDate: '',
      ExpirationYear: '',
      securityCode: ''
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.authListener();
  }

  authListener() {
    fire.auth().onAuthStateChanged(user => {
      // console.log(user);
      if (user) {
        // localStorage.setItem('user',user.uid)
      } else {
        // localStorage.removeItem(user);
      }
    });
  }
  handleClick() {
    this.props.history.push('/dashboard');
  }
  handleChange(name, value) {
    this.setState({ [name]: value });
  }
  render() {
    return (
      <div className="checkout">
        <h1>Shipping Information</h1>
        <Input
          type="text"
          required
          label="Card Number"
          name="cardNumber"
          value={this.state.cardNumber}
          onChange={this.handleChange.bind(this, 'cardNumber')}
        />
        <Input
          type="text"
          required
          label="Expires On"
          name="ExpirationDate"
          value={this.state.ExpirationDate}
          onChange={this.handleChange.bind(this, 'ExpirationDate')}
        />
        <Input
          type="text"
          required
          label="Year"
          name="ExpirationYear"
          value={this.state.ExpirationYear}
          onChange={this.handleChange.bind(this, 'ExpirationYear')}
        />
        <Input
          type="password"
          required
          label="Security Code"
          name="securityCode"
          value={this.state.securityCode}
          onChange={this.handleChange.bind(this, 'securityCode')}
        />

        <Button
          className="si_btn"
          label="Submit"
          raised
          primary
          onClick={this.handleClick}
        />
      </div>
    );
  }
}

export default withRouter(PaymentInfo);
