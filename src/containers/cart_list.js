import React, { Component } from 'react';
import { connect } from 'react-redux';
import { history } from '../routers/AppRouter';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import {
  removeFromCart,
  cartQuantity,
  cartSum,
  cartTotalQuantity,
  bookMark,
  fetchProduct,
  cartIncrementCounter,
  cartDecrementCounter
} from '../store/actions';
//css
import './css/cart_list.css';

//toolbox start
import '../assets/react-toolbox/theme.css';
import theme from '../assets/react-toolbox/theme';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';
import Button from 'react-toolbox/lib/button/Button';
import IconButton from 'react-toolbox/lib/button/IconButton';
import {
  Table,
  TableHead,
  TableRow,
  TableCell
} from 'react-toolbox/lib//table';
import Avatar from 'react-toolbox/lib/avatar/Avatar';
//toolbox end

class CartList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sum: 0
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleWishList = this.handleWishList.bind(this);
    this.proceedToCheckout = this.proceedToCheckout.bind(this);
  }

  handleClick(product) {
    let id = product.id;
    this.props.removeFromCart(id);
    let sortAddCartArray = JSON.parse(localStorage.getItem('AddCart'));

    //remove cart id from local Storage----
    if (sortAddCartArray.includes(id)) {
      sortAddCartArray = sortAddCartArray.filter(val => val !== id);
    }
    localStorage.setItem('AddCart', JSON.stringify(sortAddCartArray));
    //--------------------------------------

    this.props.cartTotalQuantity();
  }

  valueInc(index, value) {
    this.props.cartIncrementCounter(index, value);
    this.props.cartTotalQuantity();
  }
  valueDec(index, value) {
    this.props.cartDecrementCounter(index, value);
    this.props.cartTotalQuantity();
  }
  componentDidMount() {
    this.props.fetchProduct();
  }

  componentWillReceiveProps(props) {
    let objcart = props.actions.cart;
    let subTotal;
    objcart &&
      objcart.map(obj => {
        let qty = obj.quantity;
        let price = parseFloat(obj.price);
        let Sum = qty * price;
        subTotal = subTotal ? subTotal + Sum : Sum;
      });
    this.setState({ sum: subTotal });
  }

  handleWishList(product, index, e) {
    const item = {
      id: product.id,
      name: product.name,
      image: product.image,
      price: product.price,
      wishList: true
    };

    let sortWishListArray = JSON.parse(
      localStorage.getItem('WishList')
    ).slice();

    if (!sortWishListArray.includes(product.id)) {
      sortWishListArray.push(product.id);
    }

    localStorage.setItem('WishList', JSON.stringify(sortWishListArray));

    this.props.bookMark(item);
  }

  proceedToCheckout = () => {
    if (this.props.id !== null) {
      history.push('/checkout');
    } else {
      history.push('/login');
    }
  };
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="cart_Wrapper">
          <div className="sideLeft">
            <Table selectable={false} id="cart">
              <TableHead>
                <TableCell />
                <TableCell>Product</TableCell>
                <TableCell>Quantity</TableCell>
                <TableCell>Price</TableCell>
                <TableCell>sub Total</TableCell>
                <TableCell>Action</TableCell>
              </TableHead>
              {this.props.cartItems
                ? this.props.cartItems.map((obj, index) => {
                    let sortWishListArray = JSON.parse(
                      localStorage.getItem('WishList')
                    );
                    let isinWishList =
                      sortWishListArray && sortWishListArray.includes(obj.id);
                    let qValue = Number(obj.quantity);

                    return (
                      <TableRow key={obj.id}>
                        <TableCell>
                          <Avatar>
                            <img
                              alt=""
                              src={`https://greencommunitylaundry.herokuapp.com/api/Images/${
                                obj.image
                              }`}
                            />
                          </Avatar>
                        </TableCell>
                        <TableCell>{obj.name}</TableCell>
                        <TableCell className="pro_count">
                          {/* <input type="number" value={obj.quantity>=0 ? obj.quantity : 1 } onChange={this.valueChange.bind(this,index)} /> */}
                          <IconButton
                            onClick={this.valueInc.bind(this, index, qValue)}
                            icon="add"
                            floating
                            mini
                          />
                          <span className="cart_count">{qValue}</span>
                          <IconButton
                            onClick={this.valueDec.bind(this, index, qValue)}
                            icon="remove"
                            floating
                            mini
                          />
                        </TableCell>
                        <TableCell className="pri_count">
                          ${obj.price}
                        </TableCell>
                        <TableCell className="pri_count">
                          $
                          {obj.price * obj.quantity >= 1
                            ? obj.price * obj.quantity
                            : obj.price * 1}
                        </TableCell>
                        <TableCell>
                          <IconButton
                            icon="delete"
                            accent
                            onClick={() => this.handleClick(obj)}
                          />
                          <IconButton
                            className="favourite"
                            icon="favorite"
                            disabled={isinWishList}
                            accent
                            onClick={() => this.handleWishList(obj, index)}
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })
                : null}
            </Table>
          </div>
          <div className="rightside">
            <Table selectable={false} id="cart-total">
              <TableRow>
                <TableCell>
                  <b>Quantity:</b>
                </TableCell>
                <TableCell>
                  <b>
                    {this.props.qty
                      ? this.props.qty
                      : this.props.cartItems.length}{' '}
                    units
                  </b>
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <b>Total:</b>
                </TableCell>
                <TableCell id="sumV" ref="cartSum">
                  <b>${this.state.sum}</b>
                </TableCell>
              </TableRow>
            </Table>

            <Link to={this.props.id !== null ? '/checkout' : '/login'}>
              <Button
                className="chk_btn"
                disabled={!this.props.cartItems.length > 0}
                label="PROCEED TO CHECKOUT"
                raised
                primary
                // onClick={this.proceedToCheckout}
              />
            </Link>
          </div>
        </div>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = state => ({
  items: state.product.items,
  actions: state.product,
  cartItems: state.product.cart,
  wishListItems: state.product.bookmark,
  sum: state.product.cartsum,
  qty: state.product.cartTotalQuantity,
  id: state.auth.uid
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      removeFromCart,
      cartQuantity,
      cartSum,
      cartTotalQuantity,
      bookMark,
      fetchProduct,
      cartIncrementCounter,
      cartDecrementCounter
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CartList);
