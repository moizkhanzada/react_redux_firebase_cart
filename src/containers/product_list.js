import React,{Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchProduct, fetchCategories, addToCart, bookMark} from '../store/actions'
import {Link} from 'react-router-dom';
//firebase
import fire from 'firebase';
//css
import './css/product_list.css';

//toolbox start
import '../assets/react-toolbox/theme.css';
import theme from '../assets/react-toolbox/theme';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';
import {Card, CardMedia, CardTitle, CardText, CardActions} from 'react-toolbox/lib/card';
import IconButton from 'react-toolbox/lib/button/IconButton';
//toolbox end
 

class ProductList extends Component{
    constructor(props){
        super(props);
        this.state={
            category:'All',
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleWishList = this.handleWishList.bind(this);
        this.filterbyCategory = this.filterbyCategory.bind(this);
    }


    componentDidMount(){
        this.props.fetchProduct();
        this.props.fetchCategories();
    }
    
    handleClick(product){
        let cartItems =this.props.cartItems;
        let obj = cartItems.find(function (obj) { return obj.id === product._id; });
        let AddCartArray = [product._id];
        if(localStorage.getItem('AddCart') === null){
            localStorage.setItem('AddCart',JSON.stringify(AddCartArray));
        }else{
            let sortAddCartArray = JSON.parse(localStorage.getItem('AddCart')).slice();
            if(!sortAddCartArray.includes(product._id)){
                sortAddCartArray.push(product._id)
            }
            localStorage.setItem('AddCart', JSON.stringify(sortAddCartArray));
        }

       if(obj==null){
        const item = {
            id: product._id,    
            name: product.name,
            image: product.image,
            price: product.price,
            addToCart: true,
            quantity:1
        }
        this.props.addToCart(item);
    }
    };
    handleWishList(product,index,e){
        let wishList = this.props.wishListItems;
        let obj = wishList.find(function(obj){return obj.id === product._id});
       
        let wishListArray = [product._id];
        if(localStorage.getItem('WishList') === null){
            localStorage.setItem('WishList', JSON.stringify(wishListArray));
        }else{
            let sortWishListArray = JSON.parse(localStorage.getItem('WishList')).slice()
            if(!sortWishListArray.includes(product._id)){
                sortWishListArray.push(product._id) 
            }
            localStorage.setItem('WishList', JSON.stringify(sortWishListArray));
        } 
        
        if(obj==null){
            const item = {
                id: product._id,    
                name: product.name,
                image: product.image,
                price: product.price,
                wishList: true,
            }
            this.props.bookMark(item);   
        }
    }

    filterbyCategory(event,e){
        let value = e.target.value;
        this.setState({category: value});
    }

    render(){
        return(
            <ThemeProvider theme={theme}>
            {/* main */}
            <div id='main'>
            {/* sidebar */}
            <div className="sidebar">    
            </div>       
            {/* filter */}
            <div className="filter"> 
                <select className='filtbycat' onChange={(e)=>this.filterbyCategory(this,e)}>
                <option value='All'>All</option>
                {
                     this.props.catItems.map((obj)=>{
                        return(
                        <option key={obj._id} value={obj.name}>{obj.name}</option>
                        )
                })
                }
                </select>
            </div>
            <div className='divider'></div>
            {/* content */}
            <div className='content'>
            
            <div className="products">
            <ul>
            {
                this.props.items?
                this.props.items.map((obj,index)=>{
                    //trimed String starts
                    const string = obj.name;
                    const length = 21;
                    const trimedName = string.length > length ? 
                    string.substring(0, length - 3) + "..." : 
                    string; 
                    // trimed string ends
                    let objCat = obj.category;
                    
                    if(objCat.name===this.state.category){
                        return(
                            <li className="list_item"  key={obj._id}> 
                            <Card style={{width: '300px'}} >
                            <Link to={`/product_details/${obj._id}`}>
                            <CardMedia
                            aspectRatio="wide"
                            image={`https://greencommunitylaundry.herokuapp.com/api/Images/${obj.image}`}
                            />
                            </Link>
                            <Link to={`/product_details/${obj._id}`}>
                            <CardTitle
                            title={trimedName}
                            />
                            </Link>
                            <CardTitle
                            subtitle={'$'+obj.price}
                            />
                            <CardActions>
                            
                            <IconButton  disabled={obj.addToCart} onClick={() => this.handleClick(obj)} icon='add' />
                            <IconButton  disabled={obj.wishList} accent onClick={() => this.handleWishList(obj)}  className="favourite" icon='favorite'/>
                            
                            </CardActions>
                        </Card>
                        </li>
                     ) 
                    }else if(this.state.category==='All'){
                        return(
                            <li className="list_item"  key={obj._id}> 
                            <Card style={{width: '300px'}} >
                            <Link to={`/product_details/${obj._id}`}>
                            <CardMedia
                            aspectRatio="wide"
                            image={`https://greencommunitylaundry.herokuapp.com/api/Images/${obj.image}`}
                            />
                            </Link>
                            <Link to={`/product_details/${obj._id}`}>
                            <CardTitle
                            title={trimedName}
                            />
                            </Link>
                            <CardTitle
                            subtitle={'$'+obj.price}
                            />
                            <CardActions>
                            
                            <IconButton  disabled={obj.addToCart} onClick={() => this.handleClick(obj)} icon='add' />
                            <IconButton  disabled={obj.wishList} accent onClick={() => this.handleWishList(obj)}  className="favourite" icon='favorite'/>
                            
                            </CardActions>
                        </Card>
                        </li>
                     ) 
                    }
                
              }):null
             }
    
            </ul>
            </div>
            </div>
            </div>
   
            </ThemeProvider>
        )
    }
}

const mapStateToProps = (state)=>({
    items:state.product.items,
    catItems:state.product.categories,
    cartItems:state.product.cart,
    wishListItems:state.product.bookmark
}
)
function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchProduct,fetchCategories,addToCart,bookMark},dispatch);
}
export default connect(mapStateToProps,mapDispatchToProps)(ProductList);