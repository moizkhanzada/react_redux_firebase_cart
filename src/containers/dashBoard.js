import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-toolbox/lib/button/Button';
import { ThemeProvider } from 'react-css-themr';
import '../assets/react-toolbox/theme.css';
import theme from '../assets/react-toolbox/theme';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { startLogOut } from '../store/actions';

class DashBoard extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div>
          <h2>Thanks for your purchase</h2>
          <p>
            We will get back to you shortly
            <br />
            <Link to="/">
              <Button label="Back to shop" raised primary />
            </Link>
            <Button
              label="Log out"
              raised
              primary
              onClick={this.props.startLogOut}
            />
          </p>
        </div>
      </ThemeProvider>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ startLogOut }, dispatch);
}

export default connect(
  undefined,
  mapDispatchToProps
)(DashBoard);
