import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchProduct, addToCart, bookMark } from "../store/actions";
import Header from "./header";
//css
import "./css/product_dtl.css";
//toolbox start
import "../assets/react-toolbox/theme.css";
import theme from "../assets/react-toolbox/theme";
import ThemeProvider from "react-toolbox/lib/ThemeProvider";
import {
  Card,
  CardMedia,
  CardTitle,
  CardText,
  CardActions
} from "react-toolbox/lib/card";
import Button from "react-toolbox/lib/button/Button";
import IconButton from "react-toolbox/lib/button/IconButton";
import {
  Table,
  TableHead,
  TableRow,
  TableCell
} from "react-toolbox/lib//table";
//toolbox end
class ProductDeatils extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.handleClick = this.handleClick.bind(this);
    this.handleWishList = this.handleWishList.bind(this);
  }

  componentDidMount() {
    this.props.fetchProduct();
  }

  handleClick(product) {
    let cartItems = this.props.cartItems;
    let obj = cartItems.find(function(obj) {
      return obj.id === product._id;
    });

    let sortAddCartArray = JSON.parse(localStorage.getItem("AddCart")).slice();

    if (!sortAddCartArray.includes(product._id)) {
      sortAddCartArray.push(product._id);
    }

    localStorage.setItem("AddCart", JSON.stringify(sortAddCartArray));

    if (obj == null) {
      const item = {
        id: product._id,
        name: product.name,
        image: product.image,
        price: product.price,
        addToCart: true,
        quantity: 1
      };
      this.props.addToCart(item);
    }
  }
  handleWishList(product, index, e) {
    let wishList = this.props.wishListItems;
    let obj = wishList.find(function(obj) {
      return obj.id === product._id;
    });

    let sortWishListArray = JSON.parse(
      localStorage.getItem("WishList")
    ).slice();
    if (!sortWishListArray.includes(product._id)) {
      sortWishListArray.push(product._id);
    }
    localStorage.setItem("WishList", JSON.stringify(sortWishListArray));

    if (obj == null) {
      const item = {
        id: product._id,
        name: product.name,
        image: product.image,
        price: product.price,
        wishList: true
      };
      this.props.bookMark(item);
    }
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <div>
          <Header />

          <div>
            {this.props.items.map((obj, index) => {
              let queryId = this.props.match.params.pid;

              if (obj._id === queryId) {
                return (
                  <div id="main">
                    <div className="pImage">
                      <Card style={{ width: "450px" }}>
                        <CardMedia
                          aspectRatio="wide"
                          image={`https://greencommunitylaundry.herokuapp.com/api/Images/${
                            obj.image
                          }`}
                        />
                      </Card>
                      <div class="btns">
                        <Button
                          icon="add"
                          flat
                          primary
                          disabled={obj.addToCart}
                          onClick={() => this.handleClick(obj)}
                        />
                        <Button
                          icon="favorite"
                          flat
                          accent
                          disabled={obj.wishList}
                          onClick={() => this.handleWishList(obj, index)}
                        />
                      </div>
                    </div>
                    <div className="product_dtl">
                      <Table selectable={false}>
                        <TableRow>
                          <TableCell>
                            <h2>Product</h2>
                          </TableCell>
                          <TableCell>
                            <h2>{obj.name}</h2>
                          </TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>
                            <h4>Price</h4>
                          </TableCell>
                          <TableCell>{obj.price}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>
                            <h4>Category</h4>
                          </TableCell>
                          <TableCell>{obj.category.name}</TableCell>
                        </TableRow>
                        <TableRow>
                          <TableCell>
                            <h4>Description</h4>
                          </TableCell>
                          <TableCell>{obj.description}</TableCell>
                        </TableRow>
                      </Table>
                    </div>
                  </div>
                );
              } else {
                // return <div>Nothing</div>
              }
            })}
          </div>
        </div>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = state => ({
  items: state.product.items,
  cartItems: state.product.cart,
  wishListItems: state.product.bookmark
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchProduct, addToCart, bookMark }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDeatils);
