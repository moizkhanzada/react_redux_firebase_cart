import React, { Component } from 'react';
import Input from 'react-toolbox/lib/input/Input';
import Button from 'react-toolbox/lib/button/Button';
import fire from '../config/firebase';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { startLogin } from '../store/actions';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };

    this.login = this.login.bind(this);
    this.signup = this.signup.bind(this);
    this.sigInWithGoogle = this.sigInWithGoogle.bind(this);
  }

  login(e) {
    e.preventDefault();
    fire
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {})
      .catch(error => {
        console.log(error);
      });
  }

  signup(e) {
    e.preventDefault();
    fire
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .catch(error => {
        console.log(error);
      });
  }

  handleChange(name, value) {
    this.setState({ [name]: value });
  }

  sigInWithGoogle() {}

  render() {
    return (
      <section>
        <h2>Login to checkout</h2>
        <Input
          type="email"
          label="Email address"
          name="email"
          icon="email"
          value={this.state.email}
          onChange={this.handleChange.bind(this, 'email')}
        />
        <Input
          type="password"
          label="Password"
          name="password"
          icon="email"
          value={this.state.password}
          onChange={this.handleChange.bind(this, 'password')}
        />
        <Button label="Submit" onClick={this.login} />
        <Button label="Signup" onClick={this.signup} />
        <Button label="Sign in with Google" onClick={this.props.startLogin} />
      </section>
    );
  }
}

const mapStateToProps = state => ({
  id: state.auth.uid
});
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ startLogin }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
