import React,{Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { removeFromBookMark, addToCart,fetchProduct} from '../store/actions';



//toolbox start
import '../assets/react-toolbox/theme.css';
import theme from '../assets/react-toolbox/theme';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';
import Button from 'react-toolbox/lib/button/Button';
import IconButton from 'react-toolbox/lib/button/IconButton';
import { Table, TableHead, TableRow, TableCell } from 'react-toolbox/lib//table';
import Input from 'react-toolbox/lib/input/Input';
import Tooltip from 'react-toolbox/lib/tooltip/Tooltip';
import Avatar from 'react-toolbox/lib/avatar/Avatar';
//toolbox end

class FavouriteList extends Component{
    constructor(props){
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }   

    componentDidMount(){
        this.props.fetchProduct();
    }
    handleClick(product){
        let id = product.id;
        this.props.removeFromBookMark(id);
        let sortWishListArray = JSON.parse(localStorage.getItem('WishList'));  
        
        if(sortWishListArray.includes(id)){
            sortWishListArray = sortWishListArray.filter(val=> val!==id);
        }

        localStorage.setItem('WishList',JSON.stringify(sortWishListArray));
      
    }

    handleAdd(product){        
        const item = {
            id: product.id,    
            name: product.name,
            image: product.image,
            price: product.price,
            addToCart: true,
            quantity:1
        }

        let sortAddCartArray = JSON.parse(localStorage.getItem('AddCart')).slice()

        if(!sortAddCartArray.includes(product.id)){
            sortAddCartArray.push(product.id)
        }

        localStorage.setItem('AddCart',JSON.stringify(sortAddCartArray))
        


        this.props.addToCart(item);
    };

    render(){
        
        return(
            <ThemeProvider theme={theme}>
                <div className="cart_Wrapper">
                            <Table selectable={false} id='cart' >
                                <TableHead>
                                    <TableCell ><h3></h3></TableCell>
                                    <TableCell ><h3>Product</h3></TableCell>
                                    <TableCell><h3>Price</h3></TableCell>
                                    <TableCell><h3>Action</h3></TableCell>
                                    <TableCell><h3></h3></TableCell>
                                </TableHead>
                                {this.props.bookmarkItems?
                    this.props.bookmarkItems.map((obj,index)=>{
                        const sortAddCartArray= JSON.parse(localStorage.getItem('AddCart'));
                        let isInCart = sortAddCartArray.includes(obj.id);
                        return(
                                <TableRow key={obj.id} >
                                    <TableCell><Avatar><img src={`https://greencommunitylaundry.herokuapp.com/api/Images/${obj.image}`}/></Avatar></TableCell>
                                    <TableCell>{obj.name}</TableCell>
                                    <TableCell className="pri_count">${obj.price}</TableCell>
                                    <TableCell>
                                    <IconButton  icon='delete' accent onClick = {()=>this.handleClick(obj)} />
                                    <Button icon='add' disabled={isInCart} label='Add to cart' flat primary onClick = {()=>this.handleAdd(obj)} />
                                    </TableCell>
                                </TableRow>
                        ); 
                            }):null
                        }

                                
                            </Table>
                    </div>    
                           </ThemeProvider>
        )
    }
}

const mapStateToProps = (state)=>({
    items:state.product.items,
    cartItems:state.product.cart,
    bookmarkItems:state.product.bookmark
})

const mapDispatchToProps = (dispatch) =>{
    return bindActionCreators({removeFromBookMark,addToCart,fetchProduct},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(FavouriteList);