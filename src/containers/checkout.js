import React, { Component } from 'react';
import fire from '../config/firebase';
import './css/checkout.css';
import Input from 'react-toolbox/lib/input/Input';
import Autocomplete from 'react-toolbox/lib/autocomplete/Autocomplete';
import Button from 'react-toolbox/lib/button/Button';
import { withRouter } from 'react-router-dom';
class CheckOut extends Component {
  constructor(props) {
    super(props);

    this.state = {
      address1: '',
      address2: '',
      city: '',
      zipCode: '',
      country: ''
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.authListener();
  }

  authListener() {
    fire.auth().onAuthStateChanged(user => {
      // console.log(user);
      if (user) {
        // localStorage.setItem('user',user.uid)
      } else {
        // localStorage.removeItem(user);
      }
    });
  }
  handleClick() {
    this.props.history.push('/payment_Information');
  }
  handleChange(name, value) {
    this.setState({ [name]: value });
  }
  render() {
    const countriesObject = {
      'ES-es': 'Spain',
      'TH-th': 'Thailand',
      'EN-gb': 'England',
      'EN-en': 'USA'
    };
    return (
      <div className="checkout">
        <h1>Shipping Information</h1>
        <Input
          type="text"
          required
          label="Street Address"
          name="address1"
          value={this.state.address1}
          onChange={this.handleChange.bind(this, 'address1')}
        />
        <Input
          type="text"
          label="Street Address 2 (if necessary)"
          name="address2"
          value={this.state.address2}
          onChange={this.handleChange.bind(this, 'address2')}
        />
        <Input
          type="text"
          required
          label="City"
          name="city"
          value={this.state.city}
          onChange={this.handleChange.bind(this, 'city')}
        />
        <Input
          type="text"
          required
          label="ZipCode"
          name="zipCode"
          value={this.state.zipCode}
          onChange={this.handleChange.bind(this, 'zipCode')}
        />
        <Input
          type="text"
          required
          label="Country"
          name="country"
          value={this.state.country}
          onChange={this.handleChange.bind(this, 'country')}
        />
        <Button
          className="si_btn"
          label="GO TO NEXT STEP"
          raised
          primary
          onClick={this.handleClick}
        />
        {/* <Autocomplete
          direction="down"
          label="Choose country"
          source={countriesObject}
          name="country"
          value={this.state.country}
          onChange={this.handleChange.bind(this, 'country')}
        /> */}
      </div>
    );
  }
}
export default withRouter(CheckOut);
