import React, { Component } from 'react';
import { connect } from 'react-redux';
import CartIcon from '../images/CartIcon.png';
import FavIcon from '../images/favorite.png';
import { Link } from 'react-router-dom';
import { startLogOut } from '../store/actions';
// import { history } from '../routers/AppRouter';
//toolbox start
import '../assets/react-toolbox/theme.css';
import theme from '../assets/react-toolbox/theme';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';
import AppBar from 'react-toolbox/lib/app_bar/AppBar';
import Navigation from 'react-toolbox/lib/navigation/Navigation';
import Button from 'react-toolbox/lib/button/Button';
// import Link from 'react-toolbox/lib/link/Link';
//toolbox end
import './css/header.css';

class Header extends Component {
  constructor(props) {
    super(props);

    this.startLogOut = this.startLogOut.bind(this);
  }

  startLogOut() {
    startLogOut();
  }
  render() {
    const sortAddCartArray = JSON.parse(localStorage.getItem('AddCart'));
    const Counter = sortAddCartArray && sortAddCartArray.length;

    const sortWishListArray = JSON.parse(localStorage.getItem('WishList'));
    const Favourite = sortWishListArray && sortWishListArray.length;

    if (this.props.id !== null) {
      return (
        <ThemeProvider theme={theme}>
          <AppBar title="ShoppingCart" scrollHide>
            <Navigation>
              <Link to="/favourite" className="fav_link">
                <img alt="" src={FavIcon} />
                <span className="cart_favourite">{Favourite}</span>
              </Link>
              <Link to="/cart">
                <img alt="" src={CartIcon} />
                <span className="cart_counter">{Counter}</span>
              </Link>
              <Link to="/dashboard">
                <Button label="Dashboard" />
              </Link>
            </Navigation>
            <Button label="Logout" onClick={this.startLogOut} />
          </AppBar>
        </ThemeProvider>
      );
    } else {
      return (
        <ThemeProvider theme={theme}>
          <AppBar title="ShoppingCart" scrollHide>
            <Navigation>
              <Link to="/cart">
                <img alt="" src={CartIcon} />
                <span className="cart_counter">{Counter}</span>
              </Link>
              <Link to="/login">
                <Button label="Login" />
              </Link>
            </Navigation>
          </AppBar>
        </ThemeProvider>
      );
    }
  }
}

const mapStateToProps = state => ({
  cart: state.product.cart,
  favourite: state.product.bookmark,
  id: state.auth.uid
});

export default connect(mapStateToProps)(Header);
