import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import AppRouter, { history } from './routers/AppRouter';
import { login, logout } from './store/actions';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

import store from './store';

import firebase from 'firebase';

const jsx = (
  <Provider store={store}>
    <div>
      <AppRouter />
    </div>
  </Provider>

  //   document.getElementById("root")
);
registerServiceWorker();

let hasRendered = false;
const renderApp = () => {
  if (!hasRendered) {
    ReactDOM.render(jsx, document.getElementById('root'));
    hasRendered = true;
  }
};
ReactDOM.render(jsx, document.getElementById('root'));

firebase.auth().onAuthStateChanged(user => {
  if (user) {
    store.dispatch(login(user.uid));
    //     store.dispatch(startLogin()).then(() => {
    renderApp();
    if (history.location.pathname === '/') {
      // history.push("/");
      console.log('log in');
    }
    //     });
  } else {
    store.dispatch(logout());
    // renderApp();
    // history.push("/login");
  }
});
