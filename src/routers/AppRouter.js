import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import App from '../components/App';
import Cart from '../components/cart';
import favourite from '../components/favourite';
import ProductDeatils from '../containers/product_dtl';
import LoginPage from '../components/loginPage';
import DashBoardPage from '../components/dashBoardPage';
import checkOut from '../components/checkout';
import PaymentInformation from '../components/PaymentInformation';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

export const history = createHistory();
const AppRouter = () => {
  return (
    <Router history={history} basename="/shop/">
      <div>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/cart" component={Cart} />
          <PublicRoute path="/login" component={LoginPage} />
          <PrivateRoute path="/dashboard" component={DashBoardPage} />
          <PrivateRoute path="/checkout" component={checkOut} />
          <PrivateRoute path="/favourite" component={favourite} />
          <PrivateRoute
            path="/payment_Information"
            component={PaymentInformation}
          />
          <Route path="/product_details/:pid" component={ProductDeatils} />
          <Route path="*" compononent="NotFound" />
        </Switch>
      </div>
    </Router>
  );
};

export default AppRouter;
