import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
// import thunk from 'redux-thunk';
import ReduxPromise from "redux-promise";
import { logger } from "redux-logger";
import rootReducer from "./reducers";

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(ReduxPromise, logger))
);

export default store;
