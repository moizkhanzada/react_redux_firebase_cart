import firebase from 'firebase';
import axios from 'axios';

// const API_KEY = '';
const ROOT_URL = 'https://greencommunitylaundry.herokuapp.com/api/products';
const ALL_CATEGORIES =
  'https://greencommunitylaundry.herokuapp.com/api/categories/';

export const FETCH_PRODUCT = 'FETCH_PRODUCT';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const BOOK_MARK = 'BOOK_MARK';
export const CART_QUANTITY = 'CART_QUANTITY';
export const CART_SUM = 'CART_SUM';
export const CART_TOTAL_QUANTITY = 'CART_TOTAL_QUANTITY';
export const REMOVE_FROM_Book_Mark = 'REMOVE_FROM_Book_Mark';
export const FETCH_CATEGORIES = 'FETCH_CATEGORIES';
export const CART_INCREMENT_COUNTER = 'CART_INCREMENT_COUNTER';
export const CART_DECREMENT_COUNTER = 'CART_DECREMENT_COUNTER';
export const LOGOUT = 'LOGOUT';
export const LOGIN = 'LOGIN';
export const START_LOGIN = 'START_LOGIN';
export const START_LOGOUT = 'START_LOGOUT';
export const DUMMY = 'DUMMY';

export function fetchProduct() {
  const url = `${ROOT_URL}`;
  const request = axios.get(url);

  return {
    type: FETCH_PRODUCT,
    payload: request
  };
}

export function fetchCategories() {
  const url = `${ALL_CATEGORIES}`;
  const request = axios.get(url);

  return {
    type: FETCH_CATEGORIES,
    payload: request
  };
}
export function addToCart(item) {
  return {
    type: ADD_TO_CART,
    item
  };
}
export function cartQuantity(index, value) {
  return {
    type: CART_QUANTITY,
    index,
    value
  };
}

export function cartSum(value) {
  return {
    type: CART_SUM,
    value
  };
}

export function cartTotalQuantity() {
  return {
    type: CART_TOTAL_QUANTITY
  };
}
export function removeFromCart(id) {
  return {
    type: REMOVE_FROM_CART,
    id
  };
}
export function removeFromBookMark(id) {
  return {
    type: REMOVE_FROM_Book_Mark,
    id
  };
}
export function bookMark(item) {
  return {
    type: BOOK_MARK,
    item
  };
}

export function cartIncrementCounter(index, value) {
  return {
    type: CART_INCREMENT_COUNTER,
    index,
    value
  };
}

export function cartDecrementCounter(index, value) {
  return {
    type: CART_DECREMENT_COUNTER,
    index,
    value
  };
}

export const login = uid => ({
  type: LOGIN,
  uid
});

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const startLogin = () => {
  return {
    type: START_LOGIN,
    link: firebase
      .auth()
      .signInWithPopup(googleAuthProvider)
  };
};

export const logout = () => ({
  type: LOGOUT
});

export const startLogOut = () => {
  return {
    type: START_LOGOUT,
    link: firebase.auth().signOut()
  };
};

export const dummy = uid => ({
  type: DUMMY,
  uid: uid
});
