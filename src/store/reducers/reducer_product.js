import {
  FETCH_PRODUCT,
  FETCH_CATEGORIES,
  ADD_TO_CART,
  BOOK_MARK,
  REMOVE_FROM_CART,
  CART_QUANTITY,
  CART_SUM,
  CART_TOTAL_QUANTITY,
  REMOVE_FROM_Book_Mark,
  CART_INCREMENT_COUNTER,
  CART_DECREMENT_COUNTER
} from "../actions";

const initialState = {
  items: [],
  categories: [],
  cart: [],
  bookmark: [],
  details: {},
  cartsum: 0,
  cartTotalQuantity: 0,
  checkOut: ""
};

export default function(state = initialState, action) {
  const sortAddCartArray = JSON.parse(localStorage.getItem("AddCart"));
  const sortWishListArray = JSON.parse(localStorage.getItem("WishList"));

  switch (action.type) {
    case FETCH_PRODUCT:
      let data = action.payload.data ? action.payload.data.slice() : [];

      let rCart = [];
      let rFavourite = [];

      data &&
        data.map(obj => {
          if (sortAddCartArray && sortAddCartArray.includes(obj._id)) {
            if (rCart.id !== obj._id) {
              const item = {
                id: obj._id,
                name: obj.name,
                image: obj.image,
                price: obj.price,
                addToCart: true,
                quantity: 1
              };
              rCart.push(item);
            }
          }
        });

      data &&
        data.map(obj => {
          if (sortWishListArray && sortWishListArray.includes(obj._id)) {
            if (rFavourite.id !== obj._id) {
              const item = {
                id: obj._id,
                name: obj.name,
                image: obj.image,
                price: obj.price,
                addToCart: true
              };
              rFavourite.push(item);
            }
          }
        });

      data &&
        data.forEach(obj => {
          if (sortWishListArray && sortWishListArray.includes(obj._id)) {
            obj.wishList = true;
          } else {
            obj.wishList = false;
          }
        });

      data &&
        data.forEach(obj => {
          if (sortAddCartArray && sortAddCartArray.includes(obj._id)) {
            obj.addToCart = true;
          } else {
            obj.addToCart = false;
          }
        });

      return {
        ...state,
        items: data,
        cart: rCart,
        bookmark: rFavourite
      };
    case FETCH_CATEGORIES:
      return {
        ...state,
        categories: action.payload.data
      };
    case ADD_TO_CART:
      let dataCart = state.items.slice();
      for (var i = 0; i < dataCart.length; i++) {
        if (dataCart[i]._id === action.item.id) {
          dataCart[i].addToCart = true;
          break;
        }
      }
      // console.log('state.items', state.items)
      return {
        ...state,
        items: dataCart,
        cart: [...state.cart, action.item]
      };
    case CART_QUANTITY:
      let index = action.index;
      let value = action.value;

      let cartItem = state.cart.slice();
      cartItem[index].quantity = value;

      return {
        ...state,
        cart: [...state.cart]
      };

    case CART_SUM:
      console.log("....................." + action.value);
      return {
        ...state,
        cartsum: action.value
      };
    case CART_TOTAL_QUANTITY:
      let cart_Items = state.cart;
      let totalQTY;
      cart_Items.map((obj, index) => {
        let qty = obj.quantity;
        let Sum = qty;
        totalQTY = totalQTY ? totalQTY + Sum : Sum;
      });
      return {
        ...state,
        cartTotalQuantity: totalQTY >= 0 ? totalQTY : 0
      };
    case REMOVE_FROM_CART:
      return {
        ...state,
        cart: state.cart.filter(({ id }) => id !== action.id)
      };

    case BOOK_MARK:
      let dataWishList = state.items.slice();
      for (var i = 0; i < dataWishList.length; i++) {
        if (dataWishList[i]._id == action.item.id) {
          dataWishList[i].wishList = true;
          break;
        }
      }
      return {
        ...state,
        items: dataWishList,
        bookmark: [...state.bookmark, action.item]
      };
    case REMOVE_FROM_Book_Mark:
      return {
        ...state,
        bookmark: state.bookmark.filter(({ id }) => id !== action.id)
      };
    case CART_INCREMENT_COUNTER:
      let incIndex = action.index;
      let incValue = action.value + 1;
      let cartItemInc = state.cart.slice();
      cartItemInc[incIndex].quantity = incValue;

      return {
        ...state,
        cart: [...state.cart]
      };
    case CART_DECREMENT_COUNTER:
      let decIndex = action.index;
      let decValue = action.value - 1;
      let cartItemDec = state.cart.slice();
      // let check =  cartItemDec && ;
      if (cartItemDec[decIndex].quantity > 1) {
        cartItemDec[decIndex].quantity = decValue;
      }

      return {
        ...state,
        cart: [...state.cart]
      };
  }
  return state;
}
