import { LOGOUT, LOGIN } from "../actions";

export default (state = {}, action) => {
  switch (action.type) {
    case LOGIN:
      localStorage.setItem("uid", action.uid);
      return {
        uid: action.uid
      };
    case LOGOUT:
      localStorage.removeItem("uid");
      return {
        uid: null
      };
    default:
      return state;
  }
};
