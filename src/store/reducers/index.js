import { combineReducers } from "redux";
import ProductReducer from "./reducer_product";
import auth from "./reducer_auth";
const rootReducer = combineReducers({
  product: ProductReducer,
  auth: auth
});

export default rootReducer;
