import firebase from "firebase";

// Initialize Firebase
const config = {
  apiKey: "AIzaSyCXrjtmiwdtJJMepR39oK2bjeAxD3liepM",
  authDomain: "cartlogin-7eb8c.firebaseapp.com",
  databaseURL: "https://cartlogin-7eb8c.firebaseio.com",
  projectId: "cartlogin-7eb8c",
  storageBucket: "cartlogin-7eb8c.appspot.com",
  messagingSenderId: "865941331025"
};
const fire = firebase.initializeApp(config);
// const database = firebase.database();
// const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export default fire;
