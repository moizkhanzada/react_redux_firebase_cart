import React, { Component } from 'react';
import ProductList from '../containers/product_list';
import Header from '../containers/header';
//toolbox start
import '../assets/react-toolbox/theme.css';
import theme from '../assets/react-toolbox/theme';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';
//toolbox end

// css
import './App.css';

export default class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <Header />
          <ProductList />
        </div>
      </ThemeProvider>
    );
  }
}
