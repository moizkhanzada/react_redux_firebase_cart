import React, { Component } from 'react';
import CartList from "../containers/cart_list";

import Header from '../containers/header';
//toolbox start
import '../assets/react-toolbox/theme.css';
import theme from '../assets/react-toolbox/theme';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';
//toolbox end


export default class Cart extends Component{
    render(){
        return(
            <ThemeProvider theme={theme}>
              <div>
              <Header/>
              <CartList/>
              </div>
             </ThemeProvider>
        )
    }
} 