import React, { Component } from 'react';
import Login from '../containers/login';
import { connect } from 'react-redux';
import Header from '../containers/header';

//toolbox start
import '../assets/react-toolbox/theme.css';
import theme from '../assets/react-toolbox/theme';
import ThemeProvider from 'react-toolbox/lib/ThemeProvider';
//toolbox end

class LoginPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <div>
          <Header />
          <Login />
        </div>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = state => ({
  id: state.auth.uid
});

export default connect(mapStateToProps)(LoginPage);
