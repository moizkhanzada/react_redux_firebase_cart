import React, { Component } from "react";
import DashBoard from "../containers/dashBoard";

import Header from "../containers/header";
//toolbox start
import "../assets/react-toolbox/theme.css";
import theme from "../assets/react-toolbox/theme";
import ThemeProvider from "react-toolbox/lib/ThemeProvider";
//toolbox end

export default class DashBoardPage extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div>
          <Header />
          <DashBoard />
        </div>
      </ThemeProvider>
    );
  }
}
